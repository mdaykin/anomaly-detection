# TODO: Allow for changing edge detection method parameters from top level
# TODO: Use raw IR data rather than the images of the system.

import matplotlib.pyplot as plt
import imageio.v2 as imageio
import numpy as np
import cv2

from ir_image import *
from image_enhancement import *


class Img:
    def __init__(self, img_name, image_file, image_path="", image_type="ir_only", min_temp_f=0, max_temp_f=200, customer_alert_temp_f=150,
                 customer_min_temp_range_f=10, customer_threshold_to_detect_hot_spots=0.8,
                 customer_max_percentage_to_be_hot_spot=0.01, full_image_bool=True):
        self.img_name = img_name  # Name of image - can be anything
        self.image_file = image_file  # Image itself (plus path to it)
        self.image_path = image_path  # Path to image
        self.image_type = image_type  # "ir_only" or "full"
        self.min_temp_f = min_temp_f  # Read directly from Sensorcore when deployed
        self.max_temp_f = max_temp_f  # Read directly from Sensorcore when deployed
        self.full_image_bool = full_image_bool  # If True, the input is a full image. If false, it is an exported image from Sensorcore include meta details on the right of the export.
        self.customer_alert_temp_f = customer_alert_temp_f  # Customer can change the temperature at which a hot spot is found. 150F default.
        self.customer_min_temp_range_f = customer_min_temp_range_f  # Customer can change the temperature range at which under causes no hot spots to be found. 10F default.
        self.customer_threshold_to_detect_hot_spots = customer_threshold_to_detect_hot_spots  # Customer can change to between 0 and 1 to decide what fraction of image is considered for hot spots. 0.8 (default) is hottest 20% are considered for hot spots.
        self.customer_max_percentage_to_be_hot_spot = customer_max_percentage_to_be_hot_spot  # Customer can change to between 0 and 1 to detect hot spots. If 0.01 (default), then if 1% of image is detected above the customer_threshold_to_detect_hot_spots threshold, it is highlighted as a hot spot, otherwise no hot spot.
        self.image_array = self.get_img_array()
        self.plot_img_array()
        self.blurred_image = self.blur_img()

    def colour_mapping(self):
        if self.image_type == "full":
            a, b, c = self.meaned_img_lines()
            self.meaned_img_overall(a, b, c)
            self.greyscale_img(a, b, c)
        elif self.image_type == "ir_only":
            self.greyscale_img_ir_only()

    def edge_detection(self):
        self.canny_edge_detection()
        self.laplacian_edge_detection()
        self.sorbel_edge_detection()

    def get_img_array(self):
        img_array = imageio.imread(self.image_file)
        if not self.full_image_bool:
            img_array = img_array[:295, :320]
        print("img_array full shape:", img_array.shape)
        print("img_array slice shape:", img_array[:,:,0].shape)
        return img_array

    def save_img_array(self):
        if self.full_image_bool:
            imageio.imwrite("ir_image" + ".png", self.image_array)
        else:
            imageio.imwrite("ir_image" + ".png", self.image_array[:295, :320])

    def get_hot_spot(self, img):
        """
        :param img: Grey-scaled image from greyscale_img method.
        :return: Displays hot spots as output images
        """

        # First check if any part of the image is over 150'F.
        if self.max_temp_f >= 150:
            if self.min_temp_f >= 150: # Whole image too hot
                print("Entire image space has excessive heat.")
                # The formula for finding the threshold is now impossible to run, so do not continue.
            else:
                print("Area of excessive heat detected:")
                thresholding_temperature_f = self.customer_alert_temp_f

                # Formula for calculating where the threshold is:
                val_1 = thresholding_temperature_f - self.min_temp_f
                val_2 = self.max_temp_f - self.min_temp_f
                threshold = val_1 / val_2
                threshold_value = threshold * (np.max(img) - np.min(img)) + np.min(img)

                thresholded_img_vals = np.where(img >= threshold_value, img, 0)
                fig1 = plt.figure(figsize=(12, 6.4))
                ax = fig1.add_subplot(111)
                ax.spines['top'].set_visible(False)
                ax.spines['right'].set_visible(False)
                ax.spines['bottom'].set_visible(False)
                ax.spines['left'].set_visible(False)
                ax.get_xaxis().set_ticks([])
                ax.get_yaxis().set_ticks([])
                plt.imshow(thresholded_img_vals, cmap='gray', vmin=0, vmax=255)
                plt.show()
                plt.draw()
                fig1.savefig(self.image_path + "areas_of_excessive_heat_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)
                print("Now checking for high temperature variations...")
        else:
            print("No areas of excessive heat detected. Checking for high temperature variations...")

        # Second check if range of temperatures if under 10'F.
        if self.max_temp_f - self.min_temp_f < self.customer_min_temp_range_f:
            print("No significant temperature variations detected.")
            return
        else:
            print("High temperature variations detected. Checking for hot spots...")

        # Third check if fraction of "hotter area" exceeds a given fraction of whole image.

        threshold = self.customer_threshold_to_detect_hot_spots  # Between 0-1
        # value is range * threshold + min
        threshold_value = threshold * (np.max(img) - np.min(img)) + np.min(img)
        thresholded_img_vals = np.where(img >= threshold_value, 1, 0)

        nb_pixels = len(img[0]) * len(img[1])  # Multiply the lengths in each dimension
        nb_pixels_above_threshold = sum(sum(thresholded_img_vals))  # Has a 1 wherever a pixel is above the threshold

        if nb_pixels_above_threshold / nb_pixels <= self.customer_max_percentage_to_be_hot_spot:  # Less than 1% of the image
            print("Hot spots found:")
            fig1 = plt.figure(figsize=(12, 6.4))
            ax = fig1.add_subplot(111)
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_visible(False)
            ax.get_xaxis().set_ticks([])
            ax.get_yaxis().set_ticks([])
            plt.imshow(thresholded_img_vals, cmap='gray', vmin=0, vmax=1)
            plt.show()
            plt.draw()
            fig1.savefig(self.image_path + "hot_spots_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)
            return
        else:
            print("No hot spots found.")
            return


    @staticmethod
    def plot_img(img_array):
        fig = plt.figure(figsize=(6, 3.2))
        ax = fig.add_subplot(111)
        ax.set_title('colorMap')
        plt.imshow(img_array)
        ax.set_aspect('equal')

        cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
        cax.get_xaxis().set_visible(False)
        cax.get_yaxis().set_visible(False)
        cax.patch.set_alpha(0)
        cax.set_frame_on(False)
        plt.colorbar(orientation='vertical', ax=ax)
        plt.show()

    def plot_img_array(self):
        print("Image Array plot:")
        if self.full_image_bool:
            self.plot_img(self.image_array)
        else:
            print("Red:")
            self.plot_img(self.image_array[:295,:320,0])
            print("Green:")
            self.plot_img(self.image_array[:295,:320,1])
            print("Blue:")
            self.plot_img(self.image_array[:295,:320,2])
            print("All:")
            self.plot_img(self.image_array[:295, :320, :])
            print("Saving All Image as original_ir_(name).png")
            fig1 = plt.figure(figsize=(12, 6.4))
            ax = fig1.add_subplot(111)
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_visible(False)
            ax.get_xaxis().set_ticks([])
            ax.get_yaxis().set_ticks([])
            plt.imshow(self.image_array[:295, :320, :])
            plt.show()
            plt.draw()
            fig1.savefig(self.image_path + "original_ir_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)

        if self.image_type == "full":
            self.plot_img(self.image_array[4:295,350:360,0])
            self.plot_img(self.image_array[4:295,350:360,1])
            self.plot_img(self.image_array[4:295,350:360,2])

    def meaned_img_lines(self):
        a = np.array(self.image_array[3:295,354:355,0])
        b = np.array(self.image_array[3:295,354:355,1])
        c = np.array(self.image_array[3:295,354:355,2])
        a[0,:] = 255
        b[0,:] = 255
        c[0,:] = 255

        plt.plot(a, label="a")
        plt.plot(b, label="b")
        plt.plot(c, label="c")
        # C is probably Blue in RGB

        return a, b, c

    @staticmethod
    def meaned_img_overall(a, b, c):
        print("Colour Mapping + Mean:")
        d = np.concatenate((a, b, c), axis=1)
        # print("d.shape", d.shape)
        # print(np.mean(d, axis=1))

        meaned = np.mean(d, axis=1)

        plt.plot(meaned, label="mean")
        plt.legend()
        plt.show()

    def greyscale_img(self, a, b, c):
        steps = range(0, 292, 1)

        gradient = np.full(len(steps), (-255/292))

        # y = mx + c
        y = gradient*steps + 255

        if self.full_image_bool:
            img_a = self.image_array[:,:,0].copy()
            img_b = self.image_array[:,:,1].copy()
            img_c = self.image_array[:,:,2].copy()
        else:
            img_a = self.image_array[:295,:320,0].copy()
            img_b = self.image_array[:295,:320,1].copy()
            img_c = self.image_array[:295,:320,2].copy()
        img_a_copy = img_a.copy()
        img_b_copy = img_b.copy()
        img_c_copy = img_c.copy()
        a = a.reshape(292)
        b = b.reshape(292)
        c = c.reshape(292)

        threshold = 20  # Blurring parameter
        for index in a:
            img_a_copy[(img_a < index + threshold) &
                       (img_a >= index - threshold)] = y[index]

        for index in b:
            img_b_copy[(img_b < index + threshold) &
                       (img_b >= index - threshold)] = y[index]

        for index in c:
            img_c_copy[(img_c < index + threshold) &
                       (img_c >= index - threshold)] = y[index]

        # Doing this more accurately (no 2:1 mappings) to get 1 greyscale image out
        scaled_img_copy = img_c.copy()
        index = 0
        while index < len(c):
            scaled_img_copy[(img_a < a[index] + threshold) & (img_a >= a[index] - threshold) &
                            (img_b < b[index] + threshold) & (img_b >= b[index] - threshold) &
                            (img_c < c[index] + threshold) & (img_c >= c[index] - threshold)] = y[index]
            index = index+1

        # change this - maybe?

        print("Greyscaled Image with Thresholding:")
        plt.imshow(scaled_img_copy, cmap='gray', vmin=0, vmax=255)
        plt.show()

        self.get_hot_spot(scaled_img_copy)

        print("Blurred Colour A:")
        self.plot_img(img_a_copy)
        plt.show()
        print("Blurred Colour B:")
        self.plot_img(img_b_copy)
        plt.show()
        print("Blurred Colour C:")
        self.plot_img(img_c_copy)
        plt.show()

        all_together = np.concatenate((img_a_copy[:, :, np.newaxis], img_b_copy[:, :, np.newaxis], img_c_copy[:, :, np.newaxis]), axis=2)
        print("All together image:")
        self.plot_img(all_together)
        plt.show()
        meaned_image = np.mean(all_together, axis=2)
        print("Meaned Image:")
        self.plot_img(meaned_image)
        plt.show()
        print("Meaned Image (Greyscale):")
        fig1 = plt.figure(figsize=(12, 6.4))
        ax = fig1.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        plt.imshow(meaned_image, cmap='gray', vmin=0, vmax=255)
        plt.show()
        plt.draw()
        fig1.savefig(self.image_path + "meaned_img_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)

    def greyscale_img_ir_only(self):
        print("Image (Greyscale):")
        meaned_image = np.mean(self.image_array, axis=2)
        plt.imshow(meaned_image, cmap='gray', vmin=0, vmax=255)
        plt.show()
        if self.full_image_bool:
            self.get_hot_spot(meaned_image)
        else:
            self.get_hot_spot(meaned_image[:295,:320])

    def enhanced_greyscale(self):
        print("Image (Contrast-Enhanced Greyscale):")
        enhanced_greyscale_image = enhance_image("greyed_img_" + str(self.img_name) + ".png")
        fig1 = plt.figure(figsize=(12, 6.4))
        ax = fig1.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        plt.imshow(enhanced_greyscale_image, cmap='gray', vmin=0, vmax=255)
        plt.show()
        plt.draw()
        fig1.savefig(self.image_path + "enhanced_greyscale_image_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)

    def blur_img(self):
        print("Blurred Image:")
        if self.full_image_bool:
            img_col = self.image_array[:, :, :]
        else:
            img_col = self.image_array[:295,:320,:]
        #plt.imshow(img_col)
        img_gray = cv2.cvtColor(img_col, cv2.COLOR_BGR2GRAY)
        # plt.imshow(img_gray, 'gray')
        # FFT of your image to find out the frequency of that horizontal or vertical line-noise, then base Gaussian size off that
        img_blurred = cv2.GaussianBlur(img_gray,(5,5), 0)
        plt.imshow(img_blurred)
        plt.show()
        fig1 = plt.figure(figsize=(12, 6.4))
        ax = fig1.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        plt.imshow(img_blurred, cmap='gray', vmin=0, vmax=255)
        plt.show()
        plt.draw()
        fig1.savefig(self.image_path + "greyed_img_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)

        return img_blurred

    def canny_edge_detection(self):
        print("Canny Edge Detection:")
        # Canny edges
        # TODO: play around with the thresholds here
        Canny_edges = cv2.Canny(image=self.blurred_image, threshold1 = 20, threshold2 = 50)
        plt.imshow(Canny_edges)
        plt.show()

    def laplacian_edge_detection(self):
        print("Laplacian Edge Detection:")
        # Laplacian edges (2nd derivative)
        Laplacian_edges = cv2.Laplacian(self.blurred_image, cv2.CV_64F)
        plt.imshow(Laplacian_edges)
        plt.show()

    def sorbel_edge_detection(self):
        import patchify
        print("Sorbel Edge Detection (top:x & bottom:y):")
        #Sobel edges - 1st derivative (x and y)
        Sobelx_edges = cv2.Sobel(self.blurred_image, cv2.CV_64F, 1, 0, ksize=3)
        Sobely_edges = cv2.Sobel(self.blurred_image, cv2.CV_64F, 0, 1, ksize=3)
        plt.imshow(Sobelx_edges)
        plt.show()
        plt.imshow(Sobely_edges)
        plt.show()


def convert_ir(file, path, device_version):
    # File is e.g. "013108004418923-A13AF-210119-165115-20210119170243-10670CCF.IR"
    # device_version is e.g. "2.0"
    # Input
    # Remove the file path by splitting then taking last split.
    test_file_name = file
    test_file_path = path
    test_device_version = device_version

    # Usage
    ir_image = IRImage.load_raw_ir_file(test_file_name, test_file_path, test_device_version)

    # Saving thermal image
    ir_image.save_thermal_image(img_format=".png")  # or '.jpg'
    # Supported Image formats: https://imageio.readthedocs.io/en/v2.8.0/formats.html

    # # Getting temperature matrix
    # print(ir_image.temp_2d)
    #
    # # Getting metadata
    # print(ir_image.metadata.to_dict())

    return ir_image

if __name__ == "__main__":
    # Example: f"python {SCRIPT} r' {filename} ' r' {fullfilename} ' 'ir_only' 99.7 20 173.6 'False'"
    arguments = sys.argv[1:]
    if arguments[5] == 'False':  # Convert from string to Boolean
        arguments[5] = False
    else:
        arguments[5] = True
    image_c1 = Img(arguments[0][2:-1], arguments[1][2:-1], image_type=arguments[2], min_temp_f=arguments[3], max_temp_f=arguments[4], full_image_bool=arguments[5])
