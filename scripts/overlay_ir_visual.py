from __future__ import print_function
import cv2
import numpy as np

MAX_FEATURES = 500
GOOD_MATCH_PERCENT = 0.15

def alignImages(im1, im2):

  # Convert images to grayscale
  im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
  im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

  # Detect ORB features and compute descriptors.
  # orb = cv2.ORB_create(MAX_FEATURES)
  # keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
  # keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)
  #
  # # Match features.
  # matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
  # matches = matcher.match(descriptors1, descriptors2, None)
  # print("matches", matches)
  #
  # # Sort matches by score
  # matches.sort(key=lambda x: x.distance, reverse=False)
  #
  # # Remove not so good matches
  # numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
  # matches = matches[:numGoodMatches]
  #
  # # Draw top matches
  # imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
  # cv2.imwrite("matches.jpg", imMatches)
  #
  # # Extract location of good matches
  # points1 = np.zeros((len(matches), 2), dtype=np.float32)
  # points2 = np.zeros((len(matches), 2), dtype=np.float32)
  #
  # for i, match in enumerate(matches):
  #   points1[i, :] = keypoints1[match.queryIdx].pt
  #   points2[i, :] = keypoints2[match.trainIdx].pt

  points1 = np.array([[40, 22], [56, 84], [120, 87], [124, 4]])
  points2 = np.array([[251,187],[275,321],[404,334],[417,154]])

  print("Here 1")

  # Find homography
  h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)

  print("Here 2")

  # Use homography
  height, width, channels = im2.shape
  print("height, width", height, width)
  im1Reg = cv2.warpPerspective(im1, h, (width, height))

  print("Here 3")

  return im1Reg, h

if __name__ == '__main__':

  # Read reference image
  refFilename = r"C:\Users\MattDaykin\OneDrive - cniguard.com\Documents\Images\bright_spot_detection\Overlay\vs.jpg"
  print("Reading reference image : ", refFilename)
  imReference = cv2.imread(refFilename, cv2.IMREAD_COLOR)

  # Read image to be aligned
  imFilename = r"C:\Users\MattDaykin\OneDrive - cniguard.com\Documents\Images\bright_spot_detection\Overlay\ir_converted.jpg"
  print("Reading image to align : ", imFilename)
  im = cv2.imread(imFilename, cv2.IMREAD_COLOR)

  print("Aligning images ...")
  # Registered image will be resotred in imReg.
  # The estimated homography will be stored in h.
  imReg, h = alignImages(im, imReference)

  # Write aligned image to disk.
  outFilename = "aligned.jpg"
  print("Saving aligned image : ", outFilename)
  cv2.imwrite(outFilename, imReg)

  # Print estimated homography
  print("Estimated homography : \n",  h)