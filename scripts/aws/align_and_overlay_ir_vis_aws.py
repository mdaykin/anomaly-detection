import cv2
import imutils
import copy
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageEnhance, ImageOps
from imutils import contours
from skimage import measure


def pair_coordinates(x_array, y_array):
    # Takes the coordinate arrays from the bright spot detection output and puts them into the format needed for alignment

    len_x, len_y = len(x_array), len(y_array)

    assert len_x == len_y, "Array lengths (x and y) do not match. Exiting..."

    points_array = np.empty((0, 2))
    for x, y in zip(x_array, y_array):
        points_array = np.append(points_array, np.array([[x, y]]), axis=0)

    return points_array


def darken_vis_img(image, vs_img_path, vs_img_name):
    # To make the bright spots stand out in the visual image, we sometimes need to darken all other pixels BEFORE we add our bright spots.
    # This function darkens the image and saves it as a new image.

    im = Image.open(image)
    enhancer = ImageEnhance.Brightness(im)
    im_output = enhancer.enhance(0.5)
    print("saving to ", vs_img_path + vs_img_name + '_darkened.png')
    im_output.save("./static/outputs/" + vs_img_name + '_darkened.png')


def align_ir(ir_img, visual_img, alignment_points_ir, alignment_points_vs, file_name):
    # Perform homography on the IR image based on the two sets of points to align it to the visual image

    ir_img = cv2.imread(ir_img, cv2.IMREAD_COLOR)
    visual_img = cv2.imread(visual_img, cv2.IMREAD_COLOR)

    # Find homography
    h, mask = cv2.findHomography(alignment_points_ir, alignment_points_vs, cv2.RANSAC)

    # Use homography
    height, width, channels = visual_img.shape
    transformed_img = cv2.warpPerspective(ir_img, h, (width, height))

    outFilename = file_name + ".jpg"
    cv2.imwrite(outFilename, transformed_img)

    return h  # Return the homography matrix for reference.


def transperentise_aligned_img_background(aligned_img, aligned_original_ir_img, file_name):
    # Make image transparent in background. Uses both the original and the sunlight-removed image.

    aligned_img = cv2.imread(aligned_img, cv2.IMREAD_COLOR)
    aligned_original_ir_img = cv2.imread(aligned_original_ir_img, cv2.IMREAD_COLOR)

    # threshold on black to make a mask. Use the original IR aligned image to find just the border
    colour = (20, 20, 20)
    mask = np.where((aligned_original_ir_img <= colour).all(axis=2), 0, 255).astype(np.uint8)

    # put mask into alpha channel for the new aligned image
    result = aligned_img.copy()
    result = cv2.cvtColor(result, cv2.COLOR_BGR2BGRA)
    result[:, :, 3] = mask

    cv2.imwrite(file_name + '_transparent.png', result)


def overlay_ir_on_vis(aligned_ir_img, vis_img, alpha, file_name):
    aligned_ir_img = Image.open(aligned_ir_img)
    vis_img = Image.open(vis_img)

    aligned_ir_img.putalpha(int(alpha*100))
    vis_img.paste(aligned_ir_img, (0,0), aligned_ir_img)

    vis_img.save("./static/outputs/" + file_name + '_overlaid.png')

    # Now plot it
    fig1 = plt.figure(figsize=(12, 6.4))
    ax = fig1.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    plt.imshow(vis_img, cmap='gray', vmin=0, vmax=255)
    plt.show()
    plt.draw()


def add_transparent_border(img):
    img = Image.open(img)
    img = ImageOps.expand(img, border=(100, 100, 100, 100), fill=(0, 0, 0, 0))
    img.save("./static/outputs/" + "border_added_ir.png")


def alignment_point_detection(img, min_threshold_fraction=0.95, max_threshold_fraction=1.0):
    # Set alignment_spot_detection to true *only* if you are getting the coordinates of the bright spots for alignment purposes.

    print("Finding alignment points...")

    # load the image, convert it to grayscale, and blur it
    image = cv2.imread(img)
    image_copy = copy.copy(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # threshold the image to reveal light regions in the blurred image
    thresh = cv2.threshold(gray, min_threshold_fraction*np.amax(gray), max_threshold_fraction*np.amax(gray), cv2.THRESH_BINARY)[1]

    image_copy[thresh == np.max(thresh)] = 0

    # perform a connected component analysis on the thresholded image, then initialise a mask to store only the "large" components
    labels = measure.label(thresh, connectivity=2, background=0)
    mask = np.zeros(thresh.shape, dtype="uint8")
    spot_sizes_px = np.array([])  # Stores the size of each spot found in terms of number of pixes
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue
        # otherwise, construct the label mask and count the number of pixels
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
        # if the number of pixels in the component is sufficiently large, then add it to our mask of "large blobs"
        if numPixels > 4:
            mask = cv2.add(mask, labelMask)

    # find the contours in the mask, then sort them from left to right
    countours = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    countours = imutils.grab_contours(countours)
    countours = contours.sort_contours(countours)[0]

    x_array = []  # x coordinates of each contour. Used only if doing bright spot alignment.
    y_array = []  # y coordinates of each contour. Used only if doing bright spot alignment.
    # loop over the contours
    for (i, c) in enumerate(countours):
        # draw the bright spot on the image
        (x, y, w, h) = cv2.boundingRect(c)
        x_array.append(x)
        y_array.append(y)

    # Return the coordinates as these are all we are interested in.
    print("Returning array of coordinates for alignment points.")
    print("x_array", x_array)
    print("y_array", y_array)
    return x_array, y_array

def format_file_name(input_file_path):
    file_output_path = input_file_path.split("\\")[:-1]
    file_output_name = input_file_path.split("\\")[-1].split('.')[0]
    file_output_final = ''
    for string_val in file_output_path:
        file_output_final = file_output_final + string_val + '\\'
    file_output_final = file_output_final + file_output_name + '_aligned'

    return file_output_final

if __name__ == "__main__":
    # Examples: FIRST TRUE ARGUMENT IS STAGE TO RUN. Different commands (and examples) for each stage.
    # darken_img: f"python {SCRIPT} 'darken_img' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_visual_2022_04_16_original.jpg ' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\\ ' r' A1FFC_cam_3_visual_2022_04_16_original '
    # align_ir: f"python {SCRIPT} 'align_ir' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_ir_darkened_alignment_added.png ' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_visual_2022_04_16_original_darkened_alignment_added.png ' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_ir.png ' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_visual_2022_04_16_original.jpg '
    # overlay_ir: f"python {SCRIPT} 'overlay_ir' r' C:\Users\MattDaykin\PycharmProjects\ConEd - T_Demo_Dec_22\A1FFC_cam_3_ir.png ' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_visual_2022_04_16_original.jpg ' r' C:\Users\MattDaykin\PycharmProjects\ConEd-T_Demo_Dec_22\A1FFC_cam_3_visual_2022_04_16_original_darkened.png '

    arguments = sys.argv[1:]

    if arguments[0] == 'darken_img':  # Convert from string to Boolean
        darken_vis_img(image=arguments[1], vs_img_path=arguments[2], vs_img_name=arguments[3])
    elif arguments[0] == 'align_ir':
        # Get alignment point coordinates from both images
        x_array_ir, y_array_ir = alignment_point_detection(img=arguments[1], min_threshold_fraction=0.98, max_threshold_fraction=1.0)
        x_array_vs, y_array_vs = alignment_point_detection(img=arguments[2], min_threshold_fraction=0.98, max_threshold_fraction=1.0)

        # Now pair the coordinates (put them into the format required for the later work)
        alignment_points_ir = pair_coordinates(x_array_ir, y_array_ir)
        alignment_points_vs = pair_coordinates(x_array_vs, y_array_vs)

        ir_img_original_file_output_final = format_file_name(arguments[3])
        align_ir(ir_img=arguments[3], visual_img=arguments[4], alignment_points_ir=alignment_points_ir, alignment_points_vs=alignment_points_vs, file_name=ir_img_original_file_output_final)
    elif arguments[0] == 'overlay_ir':
        ir_img_original_file_output_final = format_file_name(arguments[1])
        ir_img_original_file_output_final_aligned = ir_img_original_file_output_final + ".jpg"
        transperentise_aligned_img_background(ir_img_original_file_output_final_aligned, ir_img_original_file_output_final_aligned, ir_img_original_file_output_final)
        aligned_transparent_ir_img_original = ir_img_original_file_output_final + "_transparent.png"
        overlay_ir_on_vis(aligned_transparent_ir_img_original, arguments[2], alpha=0.7, file_name=ir_img_original_file_output_final)
        overlay_ir_on_vis(aligned_transparent_ir_img_original, arguments[3], alpha=0.7, file_name=ir_img_original_file_output_final + "_dark_vs")
        # TODO: ADD TRIPLE OVERLAYS BACK IN FROM NOTEBOOK
    else:
        print("Unable to find stage to run. Check spelling. Allowed options: 'darken_img', 'align_ir', 'overlay_ir'")