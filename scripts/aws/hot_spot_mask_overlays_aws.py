import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt

from main_aws import *

def segment_mask(original_image, mask, mask_temperature_f, file_location):
    # load images
    original_img = cv2.imread(original_image)
    mask = cv2.imread(mask)

    # Convert to HSV - SWAP THESE BETWEEN CONVERSIONS
    grey_original = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY)
    grey_original = grey_original[:295, :320]
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2HSV)
    mask = mask[:295, :320]

    # define range wanted color in HSV (this segments the green colour on the mask)
    lower_val = np.array([40,250,250])
    upper_val = np.array([80,255,255])

    # DO THESE THREE BLOCKS WHEN MAKING THE INITIAL MASKS
    # Threshold the HSV image - any green color will show up as white
    mask = cv2.inRange(mask, lower_val, upper_val)

    # Taking a matrix of size 5 as the kernel
    kernel = np.ones((5, 5), np.uint8)

    # dilate and erode to fix gaps in mask
    mask = cv2.dilate(mask, kernel, iterations=2)
    mask = cv2.erode(mask, kernel, iterations=2)

    # Overlay masks
    new_img = np.where(mask != 0, grey_original, 0)

    fig1 = plt.figure(figsize=(12, 6.4))
    ax = fig1.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    plt.imshow(new_img, cmap='gray', vmin=0, vmax=255)
    plt.show()
    plt.draw()
    fig1.savefig("./static/outputs/" + str(mask_temperature_f) + "_mask.png", bbox_inches='tight', pad_inches=0)

    fig1 = plt.figure(figsize=(12, 6.4))
    ax = fig1.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    plt.imshow(grey_original, cmap='gray', vmin=0, vmax=255)
    plt.show()
    plt.draw()
    fig1.savefig("./static/outputs/" + "all_cables.png", bbox_inches='tight', pad_inches=0)

def get_hot_areas(name, file_path, image_type, image_path, min_temp_f, max_temp_f, full_image_bool, customer_alert_temp_f):
    image_c1 = Img(img_name=name, image_file=file_path, image_path=image_path, image_type=image_type, min_temp_f=min_temp_f, max_temp_f=max_temp_f, customer_alert_temp_f=customer_alert_temp_f, full_image_bool=full_image_bool)
    image_c1.colour_mapping()

def get_hot_area_x_mask(mask_cable, mask_areas_of_excess_heat, mask_hot_spots, file_location, cable_name):
    mask_areas_of_excess_heat = cv2.imread(mask_areas_of_excess_heat)
    mask_hot_spots = cv2.imread(mask_hot_spots)
    mask_cable = cv2.imread(mask_cable)

    # Convert to HSV - SWAP THESE BETWEEN CONVERSIONS
    mask_cable = cv2.cvtColor(mask_cable, cv2.COLOR_BGR2GRAY)
    mask_areas_of_excess_heat = cv2.cvtColor(mask_areas_of_excess_heat, cv2.COLOR_BGR2GRAY)
    mask_hot_spots = cv2.cvtColor(mask_hot_spots, cv2.COLOR_BGR2GRAY)

    # Overlay masks
    mask_areas_of_excess_heat = np.where(mask_areas_of_excess_heat != 0, mask_cable, 0)
    mask_hot_spots = np.where(mask_hot_spots != 0, mask_cable, 0)

    # erode and dilate to tidy output
    kernel = np.ones((5, 5), np.uint8)
    mask_areas_of_excess_heat = cv2.dilate(mask_areas_of_excess_heat, kernel, iterations=3)
    mask_areas_of_excess_heat = cv2.erode(mask_areas_of_excess_heat, kernel, iterations=3)
    mask_hot_spots = cv2.dilate(mask_hot_spots, kernel, iterations=3)
    mask_hot_spots = cv2.erode(mask_hot_spots, kernel, iterations=3)

    fig1 = plt.figure(figsize=(12, 6.4))
    ax = fig1.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    plt.imshow(mask_areas_of_excess_heat, cmap='gray', vmin=0, vmax=255)
    plt.show()
    plt.draw()
    fig1.savefig("./static/outputs/" + str(cable_name) + "_masked_areas_of_excessive_heat.png", bbox_inches='tight', pad_inches=0)

    fig1 = plt.figure(figsize=(12, 6.4))
    ax = fig1.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    plt.imshow(mask_hot_spots, cmap='gray', vmin=0, vmax=255)
    plt.show()
    plt.draw()
    fig1.savefig("./static/outputs/" + str(cable_name) + "_masked_hot_spots.png", bbox_inches='tight', pad_inches=0)

if __name__ == "__main__":
    # TODO: NEEDS TESTING TO ENSURE THE FILE NAMES LINK UP CORRECTLY - SET UP OUTPUTS ON RPi TO TEST
    # Example: f"python {SCRIPT} r' {original_image} ' r' {mask_xxxf} ' mask_temperature_xxxf r' {file_location} ' r' {name} ' 'ir_only' min_temp_f max_temp_f 'False' customer_alert_temp_f r' {mask_cable_xxx} ' r' {mask_areas_of_excess_heat_xxx} ' r' {mask_hot_spots_xxx} ' 'cable_name'"
    arguments = sys.argv[1:]
    if arguments[8] == 'False':  # Convert from string to Boolean
        arguments[8] = False
    else:
        arguments[8] = True
    segment_mask(original_image=arguments[0][2:-1], mask=arguments[1][2:-1], mask_temperature_f=float(arguments[2]), file_location=arguments[3][2:-1])
    get_hot_areas(name=arguments[4][2:-1], file_path=arguments[0][2:-1], image_type=arguments[5], image_path=arguments[3][2:-1], min_temp_f=float(arguments[6]),
                  max_temp_f=float(arguments[7]), full_image_bool=arguments[8], customer_alert_temp_f=float(arguments[9]))
    get_hot_area_x_mask(mask_cable=arguments[10][2:-1], mask_areas_of_excess_heat=arguments[11][2:-1], mask_hot_spots=arguments[12][2:-1], file_location=arguments[3][2:-1], cable_name=arguments[13])
