import sys
import cv2
from tools_aws import *

def enhance_image(image):
    # Improves the contrast of an image. In particular enhances dark areas. Some slight blurring occurs too, but it helps remove noise. Unfortunately, colour gets lost and this cannot be fixed.
    img = clahe_histogram_equilisation(image)
    new_image = crimmins(img)
    new_image = crimmins(new_image)
    return new_image

def write_image(image, file_output_name):
    print(f"Saving image as '{file_output_name}_enhanced.jpg'")
    cv2.imwrite("./static/outputs/" + file_output_name + "_enhanced.jpg", image)

if __name__ == "__main__":
    arguments = sys.argv[1:]
    image = arguments[0][2:-1]
    _, file_output_name = split_file_path(image)
    enhanced_image = enhance_image(image)
    write_image(enhanced_image, file_output_name)
