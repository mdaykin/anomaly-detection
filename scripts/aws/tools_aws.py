import cv2
import math
from PIL import Image, ImageEnhance, ImageOps

def adjust_brightness_vis_img(image, amount=1.3):
    # This function changes the brightness in the image and saves it as a new image.
    # amount = 1 means keep same. 0 means no brightness. >1 means more brightness.

    im = Image.open(image)
    enhancer = ImageEnhance.Brightness(im)
    im_output = enhancer.enhance(amount)


def adjust_contrast_vis_img(image, amount=0.7):
    # This function changes the contrast in the image and saves it as a new image.
    # amount = 1 means keep same. 0 means no contrast. >1 means more contrast.

    im = Image.open(image)
    enhancer = ImageEnhance.Contrast(im)
    im_output = enhancer.enhance(amount)
    im_output.save('contrast-adjusted-image.png')


def brighten_only_dark_regions(image):
    # Boosts the brightness of all regions but especially the darkest ones. Version 1.

    # Open the image
    im = Image.open(image)

    # Convert to HSV colourspace and split channels for ease of separate processing
    H, S, V = im.convert('HSV').split()

    # Increase the brightness, or Value channel
    strength = 200
    newV = V.point(lambda i: i + int(strength * (200 - i) / 500))

    # Recombine channels and convert back to RGB
    res = Image.merge(mode="HSV", bands=(H, S, newV)).convert('RGB')


def brighten_only_dark_regions_2(image):
    # Boosts the brightness of all regions but especially the darkest ones. Version 2.
    def lighten(img, value=70):
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        v = hsv[..., 2]
        # v[:] = cv2.add(v, value)

        # Increase the brightness, or Value channel
        strength = 50
        newV = v.point(lambda i: i + int(strength * (255 - i) / 255))

        # Recombine channels and convert back to RGB
        res = Image.merge(mode="HSV", bands=(H, S, newV)).convert('RGB')

        return cv2.cvtColor(res, cv2.COLOR_HSV2BGR)

    img = cv2.imread(image)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    mask = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY)[1] == 0
    img[mask] = lighten(img)[mask]


def brighten_only_dark_regions_3(image):
    # Boosts the brightness of all regions but especially the darkest ones. Version 3.
    # Open the image
    im = Image.open(image)

    # Convert to HSV colourspace and split channels for ease of separate processing
    H, S, V = im.convert('HSV').split()

    # Increase the brightness, or Value channel
    # TODO: Replace with s-shaped curve
    strength = 255
    curve_start = 0
    newV = V.point(lambda i: -128 + i + int((1/(1 + math.exp(-((10 * 120 / 255) - curve_start))))*strength))
    # print(1/(1 + math.exp(-((10 * 0 / 255) - curve_start))))
    # print(1/(1 + math.exp(-((10 * 120 / 255) - curve_start))))
    # print(1/(1 + math.exp(-((10 * 255 / 255) - curve_start))))


    # Recombine channels and convert back to RGB
    res = Image.merge(mode="HSV", bands=(H, S, newV)).convert('RGB')

def histogram_equilisation(image):
    src = cv2.imread(image)
    src = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    dst = cv2.equalizeHist(src)


def clahe_histogram_equilisation(image):
    src = cv2.imread(image)
    src = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=10.0, tileGridSize=(4, 4))  # Tested with lots of parameters and this seems like the best combination
    dst = clahe.apply(src)
    return dst


def crimmins(data):
    # Crimmins noise removal algorithm
    new_image = data.copy()
    nrow = len(data)
    ncol = len(data[0])

    # Dark pixel adjustment

    # First Step
    # N-S
    for i in range(1, nrow):
        for j in range(ncol):
            if data[i - 1, j] >= (data[i, j] + 2):
                new_image[i, j] += 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(ncol - 1):
            if data[i, j + 1] >= (data[i, j] + 2):
                new_image[i, j] += 1
    data = new_image
    # NW-SE
    for i in range(1, nrow):
        for j in range(1, ncol):
            if data[i - 1, j - 1] >= (data[i, j] + 2):
                new_image[i, j] += 1
    data = new_image
    # NE-SW
    for i in range(1, nrow):
        for j in range(ncol - 1):
            if data[i - 1, j + 1] >= (data[i, j] + 2):
                new_image[i, j] += 1
    data = new_image
    # Second Step
    # N-S
    for i in range(1, nrow - 1):
        for j in range(ncol):
            if (data[i - 1, j] > data[i, j]) and (data[i, j] <= data[i + 1, j]):
                new_image[i, j] += 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(1, ncol - 1):
            if (data[i, j + 1] > data[i, j]) and (data[i, j] <= data[i, j - 1]):
                new_image[i, j] += 1
    data = new_image
    # NW-SE
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i - 1, j - 1] > data[i, j]) and (data[i, j] <= data[i + 1, j + 1]):
                new_image[i, j] += 1
    data = new_image
    # NE-SW
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i - 1, j + 1] > data[i, j]) and (data[i, j] <= data[i + 1, j - 1]):
                new_image[i, j] += 1
    data = new_image
    # Third Step
    # N-S
    for i in range(1, nrow - 1):
        for j in range(ncol):
            if (data[i + 1, j] > data[i, j]) and (data[i, j] <= data[i - 1, j]):
                new_image[i, j] += 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(1, ncol - 1):
            if (data[i, j - 1] > data[i, j]) and (data[i, j] <= data[i, j + 1]):
                new_image[i, j] += 1
    data = new_image
    # NW-SE
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i + 1, j + 1] > data[i, j]) and (data[i, j] <= data[i - 1, j - 1]):
                new_image[i, j] += 1
    data = new_image
    # NE-SW
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i + 1, j - 1] > data[i, j]) and (data[i, j] <= data[i - 1, j + 1]):
                new_image[i, j] += 1
    data = new_image
    # Fourth Step
    # N-S
    for i in range(nrow - 1):
        for j in range(ncol):
            if (data[i + 1, j] >= (data[i, j] + 2)):
                new_image[i, j] += 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(1, ncol):
            if (data[i, j - 1] >= (data[i, j] + 2)):
                new_image[i, j] += 1
    data = new_image
    # NW-SE
    for i in range(nrow - 1):
        for j in range(ncol - 1):
            if (data[i + 1, j + 1] >= (data[i, j] + 2)):
                new_image[i, j] += 1
    data = new_image
    # NE-SW
    for i in range(nrow - 1):
        for j in range(1, ncol):
            if (data[i + 1, j - 1] >= (data[i, j] + 2)):
                new_image[i, j] += 1
    data = new_image

    # Light pixel adjustment

    # First Step
    # N-S
    for i in range(1, nrow):
        for j in range(ncol):
            if (data[i - 1, j] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(ncol - 1):
            if (data[i, j + 1] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # NW-SE
    for i in range(1, nrow):
        for j in range(1, ncol):
            if (data[i - 1, j - 1] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # NE-SW
    for i in range(1, nrow):
        for j in range(ncol - 1):
            if (data[i - 1, j + 1] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # Second Step
    # N-S
    for i in range(1, nrow - 1):
        for j in range(ncol):
            if (data[i - 1, j] < data[i, j]) and (data[i, j] >= data[i + 1, j]):
                new_image[i, j] -= 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(1, ncol - 1):
            if (data[i, j + 1] < data[i, j]) and (data[i, j] >= data[i, j - 1]):
                new_image[i, j] -= 1
    data = new_image
    # NW-SE
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i - 1, j - 1] < data[i, j]) and (data[i, j] >= data[i + 1, j + 1]):
                new_image[i, j] -= 1
    data = new_image
    # NE-SW
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i - 1, j + 1] < data[i, j]) and (data[i, j] >= data[i + 1, j - 1]):
                new_image[i, j] -= 1
    data = new_image
    # Third Step
    # N-S
    for i in range(1, nrow - 1):
        for j in range(ncol):
            if (data[i + 1, j] < data[i, j]) and (data[i, j] >= data[i - 1, j]):
                new_image[i, j] -= 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(1, ncol - 1):
            if (data[i, j - 1] < data[i, j]) and (data[i, j] >= data[i, j + 1]):
                new_image[i, j] -= 1
    data = new_image
    # NW-SE
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i + 1, j + 1] < data[i, j]) and (data[i, j] >= data[i - 1, j - 1]):
                new_image[i, j] -= 1
    data = new_image
    # NE-SW
    for i in range(1, nrow - 1):
        for j in range(1, ncol - 1):
            if (data[i + 1, j - 1] < data[i, j]) and (data[i, j] >= data[i - 1, j + 1]):
                new_image[i, j] -= 1
    data = new_image
    # Fourth Step
    # N-S
    for i in range(nrow - 1):
        for j in range(ncol):
            if (data[i + 1, j] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # E-W
    for i in range(nrow):
        for j in range(1, ncol):
            if (data[i, j - 1] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # NW-SE
    for i in range(nrow - 1):
        for j in range(ncol - 1):
            if (data[i + 1, j + 1] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    data = new_image
    # NE-SW
    for i in range(nrow - 1):
        for j in range(1, ncol):
            if (data[i + 1, j - 1] <= (data[i, j] - 2)):
                new_image[i, j] -= 1
    return new_image.copy()

def split_file_path(input_file_path):
    # Splits a file path into its name and path.
    file_output_path = input_file_path.split("/")[:-1]
    file_output_name = input_file_path.split("/")[-1].split('.')[0]
    return file_output_path, file_output_name
