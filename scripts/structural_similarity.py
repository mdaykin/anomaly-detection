import math
import sys

import imageio.v2 as imageio
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.patches import Rectangle
from patchify import patchify, unpatchify
from skimage.metrics import structural_similarity as ssim
import skimage.filters


class StructuralSimilarity:
    def __init__(self, image_file_1, image_file_2, enhanced_image_file_1=None, enhanced_image_file_2=None, window_size_x=100, window_size_y=100):
        # Note: Both enhanced_image_files must be set or both None. Not one set and not the other.
        # Each of the window sizes must be an integer.
        # NOTE: FOR REASONS UNKNOWN,WE HAVE TO SWAP X AND Y HERE:
        temp = window_size_x
        window_size_x = window_size_y
        window_size_y = temp

        self.window_size_x = window_size_x
        self.window_size_y = window_size_y
        self.img_1 = self.read_img(image_file_1)
        self.img_2 = self.read_img(image_file_2)
        self.plot_img(self.img_1)
        self.plot_img(self.img_2)

        # Get the sizes of the images to then make empty output images that we will populate with the processed patches, to show the image that the algorithm sees.
        x_shape, y_shape, _ = self.img_1.shape
        self.patch_output_img_1 = np.empty([x_shape, y_shape])
        self.patch_output_img_2 = np.empty([x_shape, y_shape])

        if enhanced_image_file_1 is not None:
            self.img_1_enhanced = self.read_img(enhanced_image_file_1)
            self.plot_img(self.img_1_enhanced)
            self.img_1_patches = self.patchify_image(self.img_1_enhanced, update_window_sizes=False)
        else:
            self.img_1_enhanced = None
        if enhanced_image_file_2 is not None:
            self.img_2_enhanced = self.read_img(enhanced_image_file_2)
            self.plot_img(self.img_2_enhanced)
            self.img_2_patches = self.patchify_image(self.img_2_enhanced,
                                                     update_window_sizes=True)  # Only update window sizes on the final run of the patchify, otherwise the two sets of patches can end up with slightly different shapes
        else:
            self.img_2_enhanced = None
        self.img_1_patches = self.patchify_image(self.img_1, update_window_sizes=False)
        self.img_2_patches = self.patchify_image(self.img_2, update_window_sizes=True)  # Only update window sizes on the final run of the patchify, otherwise the two sets of patches can end up with slightly different shapes

    @staticmethod
    def darken_img(img):
        return img  # TODO: DECIDE IF KEEPING. SEEMS TO WORK, BUT NEED TO EXPLORE FURTHER.
        darken_amount = 0.15
        img = np.array(img)
        img = np.where(img >= darken_amount, img - darken_amount, 0)
        return img

    @staticmethod
    def gaussian_blur(img):
        sigma = 8
        # apply Gaussian blur, creating a new image
        return skimage.filters.gaussian(img, sigma=(sigma, sigma), truncate=4, channel_axis=2)

    @staticmethod
    def balance_lighting(img1, img2):
        # Finds difference in average pixel values between both images, then adds the difference to the darkest image (all pixels)
        average_1 = np.mean(img1)
        average_2 = np.mean(img2)
        avg_diff = 0  # initialise
        if average_1 < average_2:
            avg_diff = average_2 - average_1
            img1 = img1 + avg_diff
        if average_2 < average_1:
            avg_diff = average_1 - average_2
            img2 = img2 + avg_diff
        return img1, img2, avg_diff

    @staticmethod
    def read_img(img_file):
        img = imageio.imread(img_file)
        return img

    @staticmethod
    def plot_img(img_array):
        fig = plt.figure(figsize=(12, 6.4))
        ax = fig.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        # ax.set_title('colorMap')
        plt.imshow(img_array)
        # ax.set_aspect('equal')
        #
        # cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
        # cax.get_xaxis().set_visible(False)
        # cax.get_yaxis().set_visible(False)
        # cax.patch.set_alpha(0)
        # cax.set_frame_on(False)
        # plt.colorbar(orientation='vertical', ax=ax)
        plt.show()

    def plot_img_draw_rect(self, img_array, x_rect_coord, y_rect_coord):
        # NOTE: FOR REASONS UNKNOWN,WE HAVE TO SWAP X AND Y HERE:
        temp = x_rect_coord
        x_rect_coord = y_rect_coord
        y_rect_coord = temp

        fig = plt.figure(figsize=(12, 6.4))
        ax = fig.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        # ax.set_title('colorMap')
        rect = Rectangle([x_rect_coord, y_rect_coord], self.window_size_y, self.window_size_x, alpha=1, facecolor='none', edgecolor = 'red', linewidth = 1.5)
        ax.add_patch(rect)
        plt.imshow(img_array)
        # ax.set_aspect('equal')
        #
        # cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
        # cax.get_xaxis().set_visible(False)
        # cax.get_yaxis().set_visible(False)
        # cax.patch.set_alpha(0)
        # cax.set_frame_on(False)
        # plt.colorbar(orientation='vertical', ax=ax)
        plt.show()

    def plot_img_draw_rect_all_similarities(self, img_array, x_rect_coords, y_rect_coords, similarities, red_amber_boundary=0.5, green_amber_boundary=0.8):
        # NOTE: FOR REASONS UNKNOWN,WE HAVE TO SWAP X AND Y HERE:
        temp = x_rect_coords
        x_rect_coords = y_rect_coords
        y_rect_coords = temp

        fig = plt.figure(figsize=(12, 6.4))
        ax = fig.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        # ax.set_title('colorMap')
        for similarity, x_rect_coord, y_rect_coord in zip(similarities, x_rect_coords, y_rect_coords):
            if similarity <= red_amber_boundary:
                rect = Rectangle([x_rect_coord, y_rect_coord], self.window_size_y-3, self.window_size_x-3, alpha=1,
                                 facecolor='none', edgecolor='red', linewidth=1.5)
                ax.add_patch(rect)
            elif similarity > red_amber_boundary and similarity <= green_amber_boundary:
                rect = Rectangle([x_rect_coord, y_rect_coord], self.window_size_y-3, self.window_size_x-3, alpha=1,
                                 facecolor='none', edgecolor='yellow', linewidth=1.5)
                ax.add_patch(rect)
            elif similarity > green_amber_boundary:
                rect = Rectangle([x_rect_coord, y_rect_coord], self.window_size_y-3, self.window_size_x-3, alpha=1,
                                 facecolor='none', edgecolor='green', linewidth=1.5)
                ax.add_patch(rect)
            else:  # E.g. if NaN due to patches matching exactly
                rect = Rectangle([x_rect_coord, y_rect_coord], self.window_size_y-3, self.window_size_x-3, alpha=1,
                                 facecolor='none', edgecolor='green', linewidth=1.5)
                ax.add_patch(rect)
        plt.imshow(img_array)
        # ax.set_aspect('equal')
        #
        # cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
        # cax.get_xaxis().set_visible(False)
        # cax.get_yaxis().set_visible(False)
        # cax.patch.set_alpha(0)
        # cax.set_frame_on(False)
        # plt.colorbar(orientation='vertical', ax=ax)
        plt.show()

    def patchify_image(self, img, update_window_sizes=False):
        # Adjust window size based on size of img.
        # Get size of image in x and y:
        try:
            img_shape = img[:, :, 0].shape
        except:
            img_shape = img[:, :].shape
        x_length = img_shape[0]
        y_length = img_shape[1]

        # Count how many times the chosen window size fits, then adjust to be as close as possible to this.
        nb_times_window_fits_x = x_length / self.window_size_x
        nb_times_window_fits_y = y_length / self.window_size_y
        # Round to get a better fit
        nb_times_window_fits_x = round(nb_times_window_fits_x)
        nb_times_window_fits_y = round(nb_times_window_fits_y)
        # Calculate new size of window then round down to nearest px to ensure it always fits
        new_window_size_x = math.floor(x_length / nb_times_window_fits_x)
        new_window_size_y = math.floor(y_length / nb_times_window_fits_y)

        try:
            img_patches = patchify(np.array(img[:, :, 0]), (new_window_size_x, new_window_size_y), step=1)
        except:
            img_patches = patchify(np.array(img[:, :]), (new_window_size_x, new_window_size_y), step=1)

        if update_window_sizes:
            self.window_size_x = new_window_size_x
            self.window_size_y = new_window_size_y

        return img_patches

    def run_ss_across_all_patches(self):
        similarities = np.array([])
        x_coords = np.array([])
        y_coords = np.array([])

        min_similarity = 1.01  # By being over 1, this will default to top left corner if images are identical (i.e. score of 1 in every patch)
        min_sim_x_loc = 0
        min_sim_y_loc = 0
        print("Calculating SS Score. Please wait...")
        for value_x in range(min(len(self.img_1_patches[:, 1, round(self.window_size_x/2), round(self.window_size_y/2)]), len(self.img_2_patches[:, 1, round(self.window_size_x/2), round(self.window_size_y/2)]))):
            if value_x % int(self.window_size_x):  # If the patch is not at a multiple of window size, skip it
                continue
            for value_y in range(min(len(self.img_1_patches[1, :, round(self.window_size_x/2), round(self.window_size_y/2)]), len(self.img_2_patches[1, :, round(self.window_size_x/2), round(self.window_size_y/2)]))):
                if value_y % int(self.window_size_y):  # If the patch is not at a multiple of window size, skip it
                    continue
                patch_1 = self.gaussian_blur(self.img_1_patches[value_x, value_y, :, :])
                patch_2 = self.gaussian_blur(self.img_2_patches[value_x, value_y, :, :])
                patch_1, patch_2, avg_diff = self.balance_lighting(patch_1, patch_2)
                patch_1 = self.darken_img(patch_1)
                patch_2 = self.darken_img(patch_2)
                similarity = self.structural_similarity(patch_1, patch_2)

                # Stitch the patches together.
                self.patch_output_img_1[value_x: value_x+self.window_size_x, value_y: value_y+self.window_size_y] = patch_1
                self.patch_output_img_2[value_x: value_x+self.window_size_x, value_y: value_y+self.window_size_y] = patch_2

                similarity = similarity * (1-avg_diff)

                if similarity < 0:  # Clamping. Negative similarities can occur in rare edge cases, but can be safely set to 0.
                    similarity = 0
                if math.isnan(similarity):
                    similarity = 1  # a NaN means the patches are identical, so set to be 1.
                if min_similarity > similarity:
                    min_similarity = similarity
                    min_sim_x_loc = value_x
                    min_sim_y_loc = value_y
                # self.plot_img(self.img_1_patches[value, value, :, :])
                similarities = np.append(similarities, round(similarity, 2))
                x_coords = np.append(x_coords, value_x)
                y_coords = np.append(y_coords, value_y)
        self.plot_img(self.patch_output_img_1)
        self.plot_img(self.patch_output_img_2)
        print(f"The lowest similarity is in the patch at ({min_sim_y_loc}:{min_sim_y_loc+self.window_size_y}, {min_sim_x_loc}:{min_sim_x_loc+self.window_size_x}) with a similarity of {round(min_similarity,2)}.")
        # Draw the square overlay for lowest similarity onto each image, one at a time.
        self.plot_img_draw_rect(self.img_1, min_sim_x_loc, min_sim_y_loc)
        self.plot_img_draw_rect(self.img_2, min_sim_x_loc, min_sim_y_loc)

        return similarities, x_coords, y_coords, self.img_1, self.img_2, self.img_1_enhanced, self.img_2_enhanced

    @staticmethod
    def structural_similarity(img1, img2, channel_axis=None):
        similarity = ssim(img1, img2, data_range=img2.max() - img2.min(), channel_axis=channel_axis)
        return similarity

if __name__ == "__main__":
    arguments = sys.argv[1:]
    green_amber_boundary = float(arguments[6])
    red_amber_boundary = float(arguments[7])
    # TODO: WORKS BUT OUTPUTS TO IMAGE VIEWER; NOW GET THE OUTPUTS AND SAVE THEM PROPERLY AS FILES. Done in the AWS version.
    if arguments[2] == 'None':  # No enhanced images added.
        arguments[2] = None  # Correct the parameters to be None rather than a string.
        arguments[3] = None  # Correct the parameters to be None rather than a string.
        ss = StructuralSimilarity(arguments[0][2:-1], arguments[1][2:-1], arguments[2], arguments[3], int(arguments[4]), int(arguments[5]))
        similarities, x_rect_coords, y_rect_coords, img_array_1, img_array_2, img_array_1_enhanced, img_array_2_enhanced = ss.run_ss_across_all_patches()
        ss.plot_img_draw_rect_all_similarities(img_array_1, x_rect_coords, y_rect_coords, similarities, red_amber_boundary, green_amber_boundary)
        ss.plot_img_draw_rect_all_similarities(img_array_2, x_rect_coords, y_rect_coords, similarities, red_amber_boundary, green_amber_boundary)
    else:
        ss = StructuralSimilarity(arguments[0][2:-1], arguments[1][2:-1], arguments[2][2:-1], arguments[3][2:-1], int(arguments[4]), int(arguments[5]))
        similarities, x_rect_coords, y_rect_coords, img_array_1, img_array_2, img_array_1_enhanced, img_array_2_enhanced = ss.run_ss_across_all_patches()
        ss.plot_img_draw_rect_all_similarities(img_array_1, x_rect_coords, y_rect_coords, similarities, red_amber_boundary, green_amber_boundary)
        ss.plot_img_draw_rect_all_similarities(img_array_2, x_rect_coords, y_rect_coords, similarities, red_amber_boundary, green_amber_boundary)
        ss.plot_img_draw_rect_all_similarities(img_array_1_enhanced, x_rect_coords, y_rect_coords, similarities, red_amber_boundary, green_amber_boundary)
        ss.plot_img_draw_rect_all_similarities(img_array_2_enhanced, x_rect_coords, y_rect_coords, similarities, red_amber_boundary, green_amber_boundary)

    mean = similarities.mean()
    print("Average similarity:", round(mean, 2))
