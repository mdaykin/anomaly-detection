"""
Creates human-interpretable text outputs for each of our algorithms. Strictly no images are output here - only text.
"""

import sys

import cv2
import matplotlib.pyplot as plt
import numpy as np

def hot_spot_chatter(hot_spot_image, american_bool=False):
    # Not for cable masking. Just for the hot spots.
    # Get areas of excessive heat and hot spot images
    # Detect region within image where the bright areas are
    # Produce text accordingly
    # american_bool set to True to produce text in American English rather than British English

    current_hotspot_number, text_output = location_finder_hot_spots(american_bool, hot_spot_image)

    # Add the number of hot spots at the start
    if current_hotspot_number == 1:
        text_output = "There is a single hot spot. " + text_output
    elif current_hotspot_number == 0:
        text_output = "There are no hot spots."
    else:
        text_output = "There are " + str(current_hotspot_number) + " hot spots. " + text_output

    print("text_output:", text_output)


def sunlight_spot_chatter(nb_spots_found, spot_sizes_px, found_sunlight, american_bool=False):
    # Produce text according to bright spots detected by the bright_spot_detection algorithm.
    # american_bool set to True to produce text in American English rather than British English

    avg_size = np.mean(spot_sizes_px)

    text_output = ""
    if not found_sunlight:
        if nb_spots_found > 0:
            text_output += "There are " + str(nb_spots_found) + " brighter areas with an average size of " + str(avg_size) + "px."
            text_output += "\n"
            text_output += "This pattern of bright spots is unlikely to be sunlight, and so the bright spots have been left within the region."
        else:
            text_output += "This pattern of bright spots is unlikely to be sunlight, and so the bright spots have been left within the region."
    else:
        text_output += "There are " + str(nb_spots_found) + " brighter areas with an average size of " + str(avg_size) + "px."
        text_output += "\n"
        text_output += "This pattern of bright spots in this image is likely to be sunlight. The sunlight has been removed before analysis continued."
        text_output += "\n"

    print("text_output:", text_output)

    # TODO: Expand to capture the full bright spot stuff in the alignment and overlapping notebooks (like overlapping the images and everything).



def location_finder_hot_spots(american_bool, hot_spot_image):
    """ Produces text output for where each hot spot is located within hot_spot_image.
        american_bool set to True produces American English output. False for British English.
    """

    # get bounding rectangles of contours
    img = cv2.imread(hot_spot_image)
    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(grey, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    bbox = [cv2.boundingRect(c) for c in contours]
    # Merge nearby rectangles to tidy up output.
    bbox = group_rectangles(bbox)
    nb_white_pixels, affected_area_size = get_bbox_counts(bbox, img)
    for x, y, w, h in bbox:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    plot_img(img)
    # Figure out where the rectangles are exactly
    img_y_len, img_x_len, _ = img.shape  # x, y, nb_layers
    # Divide the image into 9 regions:
    # Format: [[start x point, start y point], [end x point, end y point]]
    region_top_left = [[0, 0], [img_x_len / 3, img_y_len / 3]]
    region_top_centre = [[img_x_len / 3, 0], [2 * img_x_len / 3, img_y_len / 3]]
    region_top_right = [[2 * img_x_len / 3, 0], [img_x_len, img_y_len / 3]]
    region_centre_left = [[0, img_y_len / 3], [img_x_len / 3, 2 * img_y_len / 3]]
    region_true_centre = [[img_x_len / 3, img_y_len / 3], [2 * img_x_len / 3, 2 * img_y_len / 3]]
    region_centre_right = [[2 * img_x_len / 3, img_y_len / 3], [img_x_len, 2 * img_y_len / 3]]
    region_bottom_left = [[0, 2 * img_y_len / 3], [img_x_len / 3, img_y_len]]
    region_bottom_centre = [[img_x_len / 3, 2 * img_y_len / 3], [2 * img_x_len / 3, img_y_len]]
    region_bottom_right = [[2 * img_x_len / 3, 2 * img_y_len / 3], [img_x_len, img_y_len]]
    current_hotspot_number = 0
    text_output = ""
    nb_hotspots = len(bbox)  # The number of detected regions of hot spots.
    for x, y, w, h in bbox:
        text_output += "\n"
        current_hotspot_number += 1

        # Convert current_hotspot_number into the start of the text output for that hot spot.
        if current_hotspot_number % 10 == 1:
            if nb_hotspots == 1:  # Special text for if there is just a single hot spot.
                text_output = text_output + "It is located"
            else:
                if current_hotspot_number != 11:
                    text_output = text_output + "The " + str(current_hotspot_number) + "st hot spot is " + str(
                        affected_area_size[current_hotspot_number - 1]) + " (" + str(
                        nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"
                else:
                    text_output = text_output + "The " + str(current_hotspot_number) + "th hot spot is " + str(
                        affected_area_size[current_hotspot_number - 1]) + " (" + str(
                        nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"
        elif current_hotspot_number % 10 == 2:
            if current_hotspot_number != 12:
                text_output = text_output + "The " + str(current_hotspot_number) + "nd hot spot is " + str(
                    affected_area_size[current_hotspot_number - 1]) + " (" + str(
                    nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"
            else:
                text_output = text_output + "The " + str(current_hotspot_number) + "th hot spot is " + str(
                    affected_area_size[current_hotspot_number - 1]) + " (" + str(
                    nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"
        elif current_hotspot_number % 10 == 3:
            if current_hotspot_number != 13:
                text_output = text_output + "The " + str(current_hotspot_number) + "rd hot spot is " + str(
                    affected_area_size[current_hotspot_number - 1]) + " (" + str(
                    nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"
            else:
                text_output = text_output + "The " + str(current_hotspot_number) + "th hot spot is " + str(
                    affected_area_size[current_hotspot_number - 1]) + " (" + str(
                    nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"
        else:
            text_output = text_output + "The " + str(current_hotspot_number) + "th hot spot is " + str(
                affected_area_size[current_hotspot_number - 1]) + " (" + str(
                nb_white_pixels[current_hotspot_number - 1]) + "px)" + " and is located"

        # print("\n")
        # STARTS IN THE TOP LEFT
        if x <= region_top_left[1][0] and y <= region_top_left[1][1]:
            # print("Is in the top left")
            text_output += " in the upper left"

            # EXTENDS TO THE TOP RIGHT
            if (x + w) >= region_top_right[0][0]:
                # print("and extends to the top right")
                text_output += " and extends to the upper right"

                # CHECK IF EXTENDS DOWN FROM TOP RIGHT
                if (y + h) >= region_bottom_right[0][1]:
                    # print("and down to the bottom right")
                    text_output += " and down to the lower right"
                    text_output += "."
                    continue
                elif (y + h) >= region_centre_right[0][1]:
                    # print("and down to the centre right")
                    if american_bool:
                        text_output += " and down to the center right"
                    else:
                        text_output += " and down to the centre right"
                    text_output += "."
                    continue
                text_output += "."
                continue

            # EXTENDS TO THE TOP CENTRE
            elif (x + w) >= region_top_centre[0][0]:
                # print("and extends to the top centre")
                if american_bool:
                    text_output += " and extends to the upper center"
                else:
                    text_output += " and extends to the upper centre"

                # CHECK IF EXTENDS DOWN FROM TOP CENTRE
                if (y + h) >= region_bottom_centre[0][1]:
                    # print("and down to the bottom centre")
                    if american_bool:
                        text_output += " and down to the lower center"
                    else:
                        text_output += " and down to the lower centre"
                    text_output += "."
                    continue
                elif (y + h) >= region_true_centre[0][1]:
                    # print("and down to the centre")
                    if american_bool:
                        text_output += " and down to the center"
                    else:
                        text_output += " and down to the centre"
                    text_output += "."
                    continue
                text_output += "."
                continue

            # DOES NOT EXTEND ACROSS. CHECK IF EXTENDS DOWN FROM TOP LEFT
            if (y + h) >= region_bottom_left[0][1]:
                # print("and extends down to the bottom left")
                text_output += " and extends down to the lower left"
                text_output += "."
                continue
            elif (y + h) >= region_centre_left[0][1]:
                # print("and extends down to the centre left")
                if american_bool:
                    text_output += " and extends down to the center left"
                else:
                    text_output += " and extends down to the centre left"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE TOP CENTRE
        elif x <= region_top_centre[1][0] and y <= region_top_centre[1][1]:
            # print("Is in the top centre")
            if american_bool:
                text_output += " in the upper center"
            else:
                text_output += " in the upper centre"

            # EXTENDS TO THE TOP RIGHT
            if (x + w) >= region_top_right[0][0]:
                # print("and extends to the top right")
                text_output += " and extends to the upper right"

                # CHECK IF EXTENDS DOWN FROM TOP RIGHT
                if (y + h) >= region_bottom_right[0][1]:
                    # print("and down to the bottom right")
                    text_output += " and down to the lower right"
                    text_output += "."
                    continue
                elif (y + h) >= region_centre_right[0][1]:
                    # print("and down to the centre right")
                    if american_bool:
                        text_output += " and down to the center right"
                    else:
                        text_output += " and down to the centre right"
                    text_output += "."
                    continue

            # DOES NOT EXTEND ACROSS. CHECK IF EXTENDS DOWN FROM TOP CENTRE
            if (y + h) >= region_bottom_centre[0][1]:
                # print("and extends down to the bottom centre")
                if american_bool:
                    text_output += " and extends down to the lower center"
                else:
                    text_output += " and extends down to the lower centre"
                text_output += "."
                continue
            elif (y + h) >= region_true_centre[0][1]:
                # print("and extends down to the centre")
                if american_bool:
                    text_output += " and extends down to the center"
                else:
                    text_output += " and extends down to the centre"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE TOP RIGHT
        elif x <= region_top_right[1][0] and y <= region_top_right[1][1]:
            # print("Is in the top right")
            text_output += " in the upper right"

            # CHECK IF EXTENDS DOWN FROM TOP RIGHT
            if (y + h) >= region_bottom_right[0][1]:
                # print("and extends down to the bottom right")
                text_output += " and extends down to the lower right"
                text_output += "."
                continue
            elif (y + h) >= region_centre_right[0][1]:
                # print("and extends down to the centre right")
                if american_bool:
                    text_output += " and extends down to the center right"
                else:
                    text_output += " and extends down to the centre right"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE CENTRE LEFT
        if x <= region_centre_left[1][0] and y <= region_centre_left[1][1]:
            # print("Is in the centre left")
            if american_bool:
                text_output += " in the center left"
            else:
                text_output += " in the centre left"

            # EXTENDS TO THE CENTRE RIGHT
            if (x + w) >= region_centre_right[0][0]:
                # print("and extends to the centre right")
                if american_bool:
                    text_output += " and extends to the center right"
                else:
                    text_output += " and extends to the centre right"

                # CHECK IF EXTENDS DOWN FROM CENTRE RIGHT
                if (y + h) >= region_bottom_right[0][1]:
                    # print("and down to the bottom right")
                    text_output += " and down to the lower right"
                    text_output += "."
                    continue
                text_output += "."
                continue

            # EXTENDS TO THE TRUE CENTRE
            elif (x + w) >= region_true_centre[0][0]:
                # print("and extends to the centre")
                if american_bool:
                    text_output += " and extends to the center"
                else:
                    text_output += " and extends to the centre"

                # CHECK IF EXTENDS DOWN FROM TRUE CENTRE
                if (y + h) >= region_bottom_centre[0][1]:
                    # print("and down to the bottom centre")
                    if american_bool:
                        text_output += " and down to the lower center"
                    else:
                        text_output += " and down to the lower centre"
                    text_output += "."
                    continue
                text_output += "."
                continue

            # DOES NOT EXTEND ACROSS. CHECK IF EXTENDS DOWN FROM CENTRE LEFT
            if (y + h) >= region_bottom_left[0][1]:
                # print("and extends down to the bottom left")
                text_output += " and extends down to the lower left"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE TRUE CENTRE
        elif x <= region_true_centre[1][0] and y <= region_true_centre[1][1]:
            # print("Is in the centre")
            if american_bool:
                text_output += " in the center"
            else:
                text_output += " in the centre"

            # EXTENDS TO THE CENTRE RIGHT
            if (x + w) >= region_centre_right[0][0]:
                # print("and extends to the centre right")
                if american_bool:
                    text_output += " and extends to the center right"
                else:
                    text_output += " and extends to the centre right"

                # CHECK IF EXTENDS DOWN FROM CENTRE RIGHT
                if (y + h) >= region_bottom_right[0][1]:
                    # print("and down to the bottom right")
                    text_output += " and down to the lower right"
                    text_output += "."
                    continue

            # DOES NOT EXTEND ACROSS. CHECK IF EXTENDS DOWN FROM TRUE CENTRE
            if (y + h) >= region_bottom_centre[0][1]:
                # print("and extends down to the bottom centre")
                if american_bool:
                    text_output += " and extends down to the lower center"
                else:
                    text_output += " and extends down to the lower centre"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE CENTRE RIGHT
        elif x <= region_centre_right[1][0] and y <= region_centre_right[1][1]:
            # print("Is in the centre right")
            if american_bool:
                text_output += " in the center right"
            else:
                text_output += " in the centre right"

            # CHECK IF EXTENDS DOWN FROM CENTRE RIGHT
            if (y + h) >= region_bottom_right[0][1]:
                # print("and extends down to the bottom right")
                text_output += " and extends down to the lower right"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE BOTTOM LEFT
        if x <= region_bottom_left[1][0] and y <= region_bottom_left[1][1]:
            # print("Is in the bottom left")
            text_output += " in the lower left"

            # EXTENDS TO THE BOTTOM RIGHT
            if (x + w) >= region_bottom_right[0][0]:
                # print("and extends to the bottom right")
                text_output += " and extends to the lower right"
                text_output += "."
                continue

            # EXTENDS TO THE BOTTOM CENTRE
            elif (x + w) >= region_bottom_centre[0][0]:
                # print("and extends to the bottom centre")
                if american_bool:
                    text_output += " and extends to the lower center"
                else:
                    text_output += " and extends to the lower centre"
                text_output += "."
                continue
            text_output += "."
            continue

        # STARTS IN THE BOTTOM CENTRE
        elif x <= region_bottom_centre[1][0] and y <= region_bottom_centre[1][1]:
            # print("Is in the bottom centre")
            if american_bool:
                text_output += " in the lower center"
            else:
                text_output += " in the lower centre"

            # EXTENDS TO THE BOTTOM RIGHT
            if (x + w) >= region_bottom_right[0][0]:
                # print("and extends to the bottom right")
                text_output += " and extends to the lower right"
            text_output += "."
            continue

        # STARTS IN THE BOTTOM RIGHT
        elif x <= region_bottom_right[1][0] and y <= region_bottom_right[1][1]:
            # print("Is in the bottom right")
            text_output += " in the lower right"
            text_output += "."
            continue
    return current_hotspot_number, text_output


def get_bbox_counts(bbox, img):
    # Find the number of >0 (white) pixels in a set of bounding boxes (bbox) area and returns this as a proportion of the total image.
    nb_white_pixels = []
    affected_area_size = []
    for x, y, w, h in bbox:
        box = img[y:y + h, x:x + w]
        count = np.sum(box != 0)
        nb_white_pixels.append(count)

        total_img_area = img.shape[0] * img.shape[1]
        affected_area_percentage = 100*count/total_img_area
        # 0-1% is small. 1-5% is medium. >5% is large.
        if affected_area_percentage < 1:
            affected_area_size.append("small")
        elif affected_area_percentage < 5:
            affected_area_size.append("medium-sized")
        else:
            affected_area_size.append("large")
    return nb_white_pixels, affected_area_size


def union(a,b):
    x = min(a[0], b[0])
    y = min(a[1], b[1])
    w = max(a[0]+a[2], b[0]+b[2]) - x
    h = max(a[1]+a[3], b[1]+b[3]) - y
    return [x, y, w, h]


def isOverlapping1D(xmax1, xmin2, xmax2, xmin1):
    return xmax1 + 5 >= xmin2 and xmax2 + 5 >= xmin1


def _intersect(a,b):
    a1 = a[0]  # left edge
    a2 = a[1]  # top edge
    a3 = a[0] + a[2]  # right edge
    a4 = a[1] + a[3]  # bottom edge

    b1 = b[0]  # left edge
    b2 = b[1]  # top edge
    b3 = b[0] + b[2]  # right edge
    b4 = b[1] + b[3]  # bottom edge

    x_overlap = isOverlapping1D(a3, b1, b3, a1)
    y_overlap = isOverlapping1D(a4, b2, b4, a2)

    overlap = x_overlap and y_overlap

    return overlap


def group_rectangles(rec):
    """
    Union intersecting rectangles.
    Args:
        rec - list of rectangles in form [x, y, w, h]
    Return:
        list of grouped rectangles
    """
    tested = [False for i in range(len(rec))]
    final = []
    i = 0
    while i < len(rec):
        if not tested[i]:
            j = i+1
            while j < len(rec):
                if not tested[j] and _intersect(rec[i], rec[j]):
                    rec[i] = union(rec[i], rec[j])
                    tested[j] = True
                    j = i
                j += 1
            final += [rec[i]]
        i += 1

    return final


def plot_img(img):
    fig1 = plt.figure(figsize=(12, 6.4))
    ax = fig1.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    plt.imshow(img)
    plt.show()
    plt.draw()
    # fig1.savefig("original_ir_" + str(self.img_name) + ".png", bbox_inches='tight', pad_inches=0)

if __name__ == "__main__":
    arguments = sys.argv[1:]
    # TODO: NEEDS TESTING

    if arguments[0] == "hot spot":
        if arguments[2] == "True":
            arguments[2] = True
        else:
            arguments[2] = False
        hot_spot_chatter(arguments[1][2:-1], arguments[2])  # hot_spot_image, american_bool

    if arguments[1] == "sunlight spot":
        if arguments[3] == "True":
            arguments[3] = True
        else:
            arguments[3] = False
        if arguments[4] == "True":
            arguments[4] = True
        else:
            arguments[4] = False
        sunlight_spot_chatter(int(arguments[1]), float(arguments[2]), arguments[3], arguments[4])  # nb_spots_found, spot_sizes_px, found_sunlight, american_bool
