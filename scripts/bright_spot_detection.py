# import the necessary packages
from imutils import contours
from skimage import measure
import matplotlib.pyplot as plt
import numpy as np
import imutils
import cv2
import copy
import sys
from tools import split_file_path

# TODO: Add in name as an input, or get it from the img
def bright_spot_detection(img, erode_iterations=2, dilate_iterations=4, nb_pixels_required_for_blob=300,
                          min_threshold_fraction=0.8, max_threshold_fraction=1.0, blur_strength=11):

    print("Starting...")
    _, file_output_name = split_file_path(img)

    # load the image, convert it to grayscale, and blur it
    image = cv2.imread(img)
    image_copy = copy.copy(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if blur_strength > 0:
        blurred = cv2.GaussianBlur(gray, (blur_strength, blur_strength), 0)
    else:
        blurred = gray  # No blurring...

    # threshold the image to reveal light regions in the blurred image
    thresh = cv2.threshold(blurred, min_threshold_fraction*np.amax(blurred), max_threshold_fraction*np.amax(blurred), cv2.THRESH_BINARY)[1]
    thresh_1_return = thresh  # Just used for returning and plotting.

    # perform a series of erosions and dilations to remove any small blobs of noise from the thresholded image
    thresh = cv2.erode(thresh, None, iterations=erode_iterations)
    # thresh_2_return = thresh  # Just used for returning and plotting.
    thresh = cv2.dilate(thresh, None, iterations=dilate_iterations)
    thresh_2_return = thresh  # Just used for returning and plotting.
    image_copy[thresh == np.max(thresh)] = 0

    # perform a connected component analysis on the thresholded image, then initialise a mask to store only the "large" components
    labels = measure.label(thresh, connectivity=2, background=0)
    mask = np.zeros(thresh.shape, dtype="uint8")
    # loop over the unique components
    nb_spots_found = 0  # Counts up 1 for every spot found.
    spot_sizes_px = np.array([])  # Stores the size of each spot found in terms of number of pixes
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue
        # otherwise, construct the label mask and count the number of pixels
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
        # if the number of pixels in the component is sufficiently large, then add it to our mask of "large blobs"
        if numPixels > nb_pixels_required_for_blob:
            mask = cv2.add(mask, labelMask)
            nb_spots_found += 1
            spot_sizes_px = np.append(spot_sizes_px, numPixels)
    print("nb_spots_found", nb_spots_found)
    print("average spot size (px):", round(np.mean(spot_sizes_px), 0))

    # find the contours in the mask, then sort them from left to right
    if nb_spots_found > 0:
        countours = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        countours = imutils.grab_contours(countours)
        countours = contours.sort_contours(countours)[0]

        # loop over the contours
        for (i, c) in enumerate(countours):
            # draw the bright spot on the image
            (x, y, w, h) = cv2.boundingRect(c)
            ((cX, cY), radius) = cv2.minEnclosingCircle(c)
            cv2.circle(image, (int(cX), int(cY)), int(radius),
                (0, 0, 255), 3)
            # cv2.putText(image, "#{}".format(i + 1), (x, y - 15),
            #     cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 1)
    # If more than 10 bright spots found and average bright spot size is under 222px, then call this sunlight.
    if nb_spots_found >= 10 and np.mean(spot_sizes_px) <= 222:
        found_sunlight = True
        print("Found sunlight!")
        print(f"Removing sunlight and saving figure as '{file_output_name}_removed_sunlight.png'...")
        mydpi = 120/1.56  # Correction factor
        x_size = image.shape[1]/mydpi
        y_size = image.shape[0]/mydpi
        fig1 = plt.figure(figsize=(x_size, y_size))
        ax = fig1.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        plt.imshow(image_copy, cmap='gray', vmin=0, vmax=255)
        plt.show()
        plt.draw()
        fig1.savefig(str(file_output_name) + "_removed_sunlight.png", bbox_inches='tight', pad_inches=0)
    else:
        print("No sunlight found! Saving original file again with _removed_sunlight appended.")
        image_copy = image  # Undo the version that removes sunlight...
        found_sunlight = False
        mydpi = 120 / 1.56  # Correction factor
        x_size = image.shape[1] / mydpi
        y_size = image.shape[0] / mydpi
        fig1 = plt.figure(figsize=(x_size, y_size))
        ax = fig1.add_subplot(111)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        plt.imshow(image_copy, cmap='gray', vmin=0, vmax=255)
        plt.show()
        plt.draw()
        fig1.savefig(str(file_output_name) + "_removed_sunlight.png", bbox_inches='tight', pad_inches=0)

    # return the output image
    return image, thresh_1_return, thresh_2_return, image_copy, nb_spots_found, spot_sizes_px, found_sunlight


def plot_img(img_array):
    fig = plt.figure(figsize=(12, 6.4))
    ax = fig.add_subplot(111)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    # ax.set_title('colorMap')
    plt.imshow(img_array)
    # ax.set_aspect('equal')
    #
    # cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
    # cax.get_xaxis().set_visible(False)
    # cax.get_yaxis().set_visible(False)
    # cax.patch.set_alpha(0)
    # cax.set_frame_on(False)
    # plt.colorbar(orientation='vertical', ax=ax)
    plt.show()

if __name__ == "__main__":
    arguments = sys.argv[1:]
    bright_spot_detection(arguments[0][2:-1], int(arguments[1]), int(arguments[2]), int(arguments[3]), float(arguments[4]), float(arguments[5]), int(arguments[6]))
    # above parameters: img, erode_iterations, dilate_iterations, nb_pixels_required_for_blob, min_threshold_fraction=, max_threshold_fraction=, blur_strength